import React from 'react';
import { GamingChat } from './components/Chat';
import { VideoStream } from './components/VideoStream';

import { LayoutControlProvider } from './context/LayoutController';

const App = () => (
  <main>
    <LayoutControlProvider>
        {/*<VideoStream />*/}
        <div className={'video-stream chat-visible'}>
            <h1 style={{color: "white", textAlign: "center", marginTop: 200}}>
                Main Superbit content here
            </h1>
        </div>
        <GamingChat />
    </LayoutControlProvider>
  </main>
);

export default App;
